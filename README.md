# wp-basic

Learning wordpress development.

## Setup
We are following the instructions below from 
https://www.digitalocean.com/community/tutorials/how-to-install-wordpress-with-lemp-on-ubuntu-16-04

### Install nginx
<code>
`sudo apt-get update`
`sudo apt-get install nginx`
</code>
Verify that nginx is running by opening `localhost:80` on browser.

### Install mysql
> `sudo apt-get install mysql-server`

### Install php
php processor for nginx
> `sudo apt-get install php-fpm`
php plugin for using mysql
> `sudo apt-get install php-mysql`

### Configure nginx to work with php processor
Follow Step 4: Configure Nginx to Use the PHP Processor, from this link https://www.digitalocean.com/community/tutorials/how-to-install-linux-nginx-mysql-php-lemp-stack-in-ubuntu-16-04
(Note: you don't need to put a server_name for local setup)

### Testing nginx with php
Follow Step 5: Create a PHP File to Test Configuration of the same link. Note that you use `localhost:80/info.php` to test on your browser. Also no need to remove this file for now as its useful for looking at all the configuration at one place.

### Security (optional)
If you want to secure nginx http server, then follow the instructions given in : https://www.digitalocean.com/community/tutorials/how-to-create-a-self-signed-ssl-certificate-for-nginx-in-ubuntu-16-04

We are done with basic server setup. Next we move on to wordpress installation:

### Create a MySQL Database and User for WordPress
> `mysql -u root -p`
(Note: option -p is not required if you don't have a root password, which is terrible!!)

`mysql> CREATE DATABASE wordpress DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;`
Query OK, 1 row affected (0.00 sec)

`mysql> GRANT ALL ON wordpress.* TO '<your_username>'@'localhost' IDENTIFIED BY '<password>';`
Query OK, 0 rows affected, 1 warning (0.01 sec)

`mysql> FLUSH PRIVILEGES;`
Query OK, 0 rows affected (0.00 sec)

`mysql> EXIT`

### NGINX configuration for wordpress
Follow STEP2 of https://www.digitalocean.com/community/tutorials/how-to-install-wordpress-with-lemp-on-ubuntu-16-04

### Install php extensions needed by wordpress
> `sudo apt-get install php-curl php-gd php-mbstring php-mcrypt php-xml php-xmlrpc`

(Note: Each WordPress plugin has its own set of requirements. Some may require additional PHP packages to be installed. Check your plugin documentation to discover its PHP requirements. If they are available, they can be installed with apt-get as demonstrated above.)

Restart php processor so taht the new extensions work.
> `sudo service php7.0-fpm restart`

### Download wordpress
Follow STEP4 of previous link.

### Configure wordpress directory
#### Ownership and Permissions
(Note: you will find details about the commands below in STEP5 of link.)
<code>
 `sudo chown -R <your_username>:www-data /var/www/html`
 `sudo find /var/www/html -type d -exec chmod g+s {} \;`
 `sudo chmod g+w /var/www/html/wp-content/`
 `sudo chmod -R g+w /var/www/html/wp-content/themes`
 `sudo chmod g+w /var/www/html/wp-content/plugins`
</code>
#### Setting up configuration file
(Follow 'Setting up the WordPress Configuration File
' in STEP5 of the link.)

### Complete installation through web interface
Follow STEP6 of the link.

If you have come this far, you already have a working wordpress! start exploring and building your website, Cheers!

### Additional set up
#### PHPMYADMIN
> `sudo apt-get install phpmyadmin`

Edit nginx config file ` /etc/nginx/sites-available/default` and add the following to `server` block
<code>
        location /phpmyadmin {
                root /usr/share/; index index.php;
                location ~ ^/phpmyadmin/(.+\.php)$ {
                        try_files $uri =404;
                        fastcgi_index index.php;
                        fastcgi_pass unix:/run/php/php7.0-fpm.sock;
                        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
                        include fastcgi_params;
                }
                location ~* ^/phpmyadmin/(.+\.(jpg|jpeg|gif|css|png|js|ico|html|xml|txt))$
                        { root /usr/share/; }
        }

</code>

Restart services to enable phpmyadmin
<code>
 `sudo service nginx restart`
 `sudo service php7.0-fpm restart`
 `sudo service mysql restart`
</code>


